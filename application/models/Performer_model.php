<?php
/**
 * Performer model
 *
 * @method mixed get_performers()
 * @method mixed get_performer($slug)
 */
class Performer_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_performers()
    {
        $query = $this->db->select('performers.id, act_name, url, thumbnail, city_name, state_code, state_name, country_code, name as category_name')->join('categories', 'performers.category_id = categories.id')->get('performers');
        return $query;
    }

    public function get_performers_by_category($id)
    {
        $query = $this->db->select('performers.id, category_id, act_name, url, thumbnail, city_name, state_code, state_name, country_code, name as category_name')->where('category_id', $id)->get('performers');
        return $query;
    }

    public function get_performer($slug)
    {
        $query = $this->db->select('performers.id, act_name, url, thumbnail, city_name, state_code, state_name, country_code, name as category_name')->where('url', $slug)->get('performers');
        return $query;
    }
}