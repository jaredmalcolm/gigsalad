<?php
/**
 * Category model
 *
 * @method mixed get_categories()
 * @method mixed get_category($id)
 */
class Category_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_categories()
    {
        $query = $this->db->get('categories');
        return $query;
    }

    public function get_category($id)
    {
        $query = $this->db->where('id', $id)->get('categories');
        return $query;
    }
}