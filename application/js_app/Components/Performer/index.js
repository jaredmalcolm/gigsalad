import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import find from 'lodash/find';

export const Performer = ({ match, performersLoading, allPerformers }) => {
	const targetUrl = match.params.url;
	const targetPerformer = find(
		allPerformers,
		(performer) => targetUrl === performer.url
	);
	return (
		<section className="section">
			<div className="container">
				<h1 className="title is-1">Viewing {targetPerformer.act_name}</h1>
				<article className="media">
					<figure className="media-left">
						<p className="image is-480x480">
							<img src={JSON.parse(targetPerformer.thumbnail).url} />
						</p>
					</figure>
					<div className="media-content">
						<div className="content">
                            <dl>
                                <dt>Act Name</dt>
                                <dd>{targetPerformer.act_name}</dd>
                                <dt>Entertainment Type</dt>
                                <dd>{targetPerformer.category_name}</dd>
                                <dt>City</dt>
                                <dd>{targetPerformer.city_name}</dd>
                                <dt>State</dt>
                                <dd>{targetPerformer.state_name} ({targetPerformer.state_code})</dd>
                                <dt>Country</dt>
                                <dd>{targetPerformer.country_code}</dd>
                            </dl>
						</div>
					</div>
				</article>
			</div>
		</section>
	);
};

Performer.propTypes = {
	performersLoading: PropTypes.bool.isRequired,
	allPerformers: PropTypes.array.isRequired,
};

export default Performer;
