import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Masonry from 'react-masonry-component';

import ContentCard from '../Common/ContentCard';

export const CategoryList = ({
	allCategories,
	categoriesLoading,
	setPerformerFilter,
}) => (
	<Fragment>
		<section className="section">
			<div className="container">
				<h1 className="title is-1">
					Performer List{' '}
					<em className="subtitle is-6 has-text-grey-lighter">
						Showing {allCategories.length} results!
					</em>
				</h1>
                <p className="subtitle">Choose one of the entertainment categories below to view our entertainers</p>
				<Masonry
					options={{ transitionDuration: 250 }} // default {}
					className="columns"
				>
					{allCategories.map((category) => (
						<div className="column is-one-quarter" key={category.id}>
							<ContentCard
								title={category.name}
                                linkTarget={`/performers`}
                                onClickAction={() => setPerformerFilter(category.name)}
							/>
						</div>
					))}
				</Masonry>
			</div>
		</section>
	</Fragment>
);

CategoryList.propTypes = {
	setPerformerFilter: PropTypes.func.isRequired,
	allCategories: PropTypes.array.isRequired,
	categoriesLoading: PropTypes.bool.isRequired,
};

export default CategoryList;
