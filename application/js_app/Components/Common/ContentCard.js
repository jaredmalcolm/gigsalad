import React from 'react';
import PropTypes from 'prop-types';

import { Link, withRouter } from 'react-router-dom';

const BaseCard = ({ image, title, subTitle }) => (
	<div className="card">
		{image && (
			<div className="card-image">
				<figure className="image is-1by1">
					<img src={image} alt={title + ' image'} />
				</figure>
			</div>
		)}
		<div className="card-content">
			<div className="content">
				{title} <br />
				{subTitle}
			</div>
		</div>
	</div>
);

export const ContentCard = withRouter(
	({ history, image, title, subTitle, linkTarget, onClickAction }) => {
		if (linkTarget) {
			if (typeof onClickAction !== 'undefined') {
				return (
					<a
						href={linkTarget}
						onClick={(ele) => {
							ele.preventDefault();
							onClickAction();
							history.push(linkTarget);
						}}
					>
						<BaseCard image={image} title={title} subTitle={subTitle} />
					</a>
				);
			} else {
				return (
					<Link to={linkTarget}>
						<BaseCard image={image} title={title} subTitle={subTitle} />
					</Link>
				);
			}
		} else {
			return (
				<BaseCard
					image={image}
					title={title}
					subTitle={subTitle}
					onClick={onClickAction || (() => false)}
				/>
			);
		}
	}
);

ContentCard.propTypes = {
	title: PropTypes.string.isRequired,
	subTitle: PropTypes.string,
	image: PropTypes.string,
	linkTarget: PropTypes.string,
	onClickAction: PropTypes.func,
};

export default ContentCard;
