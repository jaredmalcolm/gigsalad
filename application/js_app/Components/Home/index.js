import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import ContentCard from '../Common/ContentCard';

export const Home = (props) => (
	<Fragment>
		<section className="section">
			<div className="container">
				<h1 className="title is-1">GigSalad Demo Homepage</h1>
				<div className="box">
					<h2 className="subtitle">Featured Entertainment</h2>
					{props.performersLoading ? (
						<img src="/loading.gif" alt="Loading Featured Entertainment" />
					) : (
						<div className="columns">
							{props.featuredPerformers.map((performer) => (
								<div className="column" key={performer.url}>
									<ContentCard
										title={performer.act_name}
										subTitle={`${performer.category_name} - ${
											performer.city_name
										}, ${performer.state_code}`}
										image={JSON.parse(performer.thumbnail).url}
										linkTarget={`/performers/${performer.url}`}
									/>
								</div>
							))}
							<div className="column">
								<ContentCard
									title="View All"
									subTitle="View all of our excellent entertainment offerings!"
									linkTarget="/performers"
									onClickAction={() => props.setPerformerFilter('')}
								/>
							</div>
						</div>
					)}
				</div>
				<div className="box">
					<h2 className="subtitle">Featured Entertainment Categories</h2>
					{props.categoriesLoading ? (
						<img
							src="/loading.gif"
							alt="Loading Featured Entertainment Categories"
						/>
					) : (
						<div className="columns">
							{props.featuredCategories.map((category) => (
								<div className="column" key={category.id}>
									<ContentCard
										title={category.name}
										linkTarget={`/performers`}
										onClickAction={() => props.setPerformerFilter(category.name)}
									/>
								</div>
							))}
							<ContentCard
								title="View All"
								subTitle="View all entertainment category options!"
								linkTarget="/categories"
							/>
						</div>
					)}
				</div>
			</div>
		</section>
	</Fragment>
);

export default Home;
