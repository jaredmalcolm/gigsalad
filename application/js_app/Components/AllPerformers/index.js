import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Masonry from 'react-masonry-component';
import shuffle from 'lodash/shuffle';

import ContentCard from '../Common/ContentCard';
export const AllPerformers = ({
	performersLoading,
	filteredPerformers,
	doPerformerFilter,
	searchFilter,
}) => (
	<Fragment>
		<section className="section">
			<div className="container">
				<h1 className="title is-1">
					Performer List{' '}
					<em className="subtitle is-6 has-text-grey-lighter">
						Showing {filteredPerformers.length} results!
					</em>
				</h1>
				<input
					className="input"
					type="text"
					placeholder="Search by Act Name, City, State, Entertainment Type, etc."
					onChange={doPerformerFilter}
					value={searchFilter}
				/>
			</div>
		</section>
		<section className="section">
			<div className="container">
				<Masonry
					options={{ transitionDuration: 250 }} // default {}
					className="columns"
				>
					{shuffle(filteredPerformers).map((performer) => (
						<div className="column is-one-quarter" key={performer.url}>
							<ContentCard
								title={performer.act_name}
								subTitle={`${performer.category_name} - ${
									performer.city_name
								}, ${performer.state_code}`}
								image={JSON.parse(performer.thumbnail).url}
								linkTarget={`/performers/${performer.url}`}
							/>
						</div>
					))}
				</Masonry>
			</div>
		</section>
	</Fragment>
);

AllPerformers.propTypes = {
	performersLoading: PropTypes.bool.isRequired,
	filteredPerformers: PropTypes.array.isRequired,
	searchFilter: PropTypes.string.isRequired,
	doPerformerFilter: PropTypes.func,
};

export default AllPerformers;
