import React, { Component, Fragment } from 'react';
import { Link, NavLink, Route, Switch } from 'react-router-dom';

import axios from 'axios';
import sampleSize from 'lodash/sampleSize';
import filter from 'lodash/filter';

import AllPerformers from '../AllPerformers';
import CategoryList from '../CategoryList';
import Home from '../Home';
import Performer from '../Performer';

export class App extends Component {
	numToFeature = 3;
	state = {
		mobileNavOpen: false,
		performersLoading: false,
		categoriesLoading: false,
		searchFilter: '',
		allCategories: [],
		allPerformers: [],
		filteredPerformers: [],
		featuredPerformers: [],
		featuredCategories: [],
	};
	toggleNavMenu = (targetState) => {
		this.setState({
			...this.state,
			mobileNavOpen: targetState || !this.state.mobileNavOpen,
		});
	};
	getAllPerformers = () => {
		axios
			.get('/api/performers')
			.then((response) => {
				this.setState({
					...this.state,
					performersLoading: false,
					allPerformers: response.data,
					filteredPerformers: response.data,
					featuredPerformers: sampleSize(response.data, this.numToFeature),
				});
			})
			.catch((error) => {
				this.setState({
					...this.state,
					performersLoading: false,
				});
				console.error(error);
			});
	};
	getAllCategories = () => {
		axios
			.get('/api/categories')
			.then((response) => {
				this.setState({
					...this.state,
					categoriesLoading: false,
					allCategories: response.data,
					featuredCategories: sampleSize(response.data, this.numToFeature),
				});
			})
			.catch((error) => {
				this.setState({
					...this.state,
					categoriesLoading: false,
				});
				console.error(error);
			});
	};
	componentDidMount = () => {
		this.setState({
			...this.state,
			performersLoading: true,
			categoriesLoading: true,
		});
		this.getAllPerformers();
		this.getAllCategories();
	};
	doPerformerFilter = (ele) => {
		this.setState({
			...this.state,
			searchFilter: ele.target.value,
			filteredPerformers: filter(
				this.state.allPerformers,
				(p) =>
					`${p.act_name}${p.category_name}${p.city_name}${p.country_code}${
						p.state_code
					}${p.state_name}`
						.toLowerCase()
						.indexOf(ele.target.value.toLowerCase()) !== -1
			),
		});
	};
	setPerformerFilter = (searchTerm) => {
		this.setState({
			...this.state,
			searchFilter: searchTerm,
			filteredPerformers: filter(
				this.state.allPerformers,
				(p) =>
					`${p.act_name}${p.category_name}${p.city_name}${p.country_code}${
						p.state_code
					}${p.state_name}`
						.toLowerCase()
						.indexOf(searchTerm.toLowerCase()) !== -1
			),
		});
	};
	render() {
		return (
			<Fragment>
				<nav
					className={this.state.mobileNavOpen ? 'navbar is-active' : 'navbar'}
					role="navigation"
					aria-label="main navigation"
				>
					<div className="container">
						<div className="navbar-brand">
							<Link to="/" className="navbar-item">
								<img
									src="https://www.gigsalad.com/images/logo.svg"
									alt="GigSalad demo app"
									width="112"
									height="112"
									style={{ maxHeight: '4.75rem' }}
								/>
							</Link>

							<a
								role="button"
								className={
									this.state.mobileNavOpen
										? 'navbar-burger is-active'
										: 'navbar-burger'
								}
								aria-label="menu"
								aria-expanded={this.state.mobileNavOpen}
								onClick={this.toggleNavMenu}
							>
								<span aria-hidden="true" />
								<span aria-hidden="true" />
								<span aria-hidden="true" />
							</a>
						</div>
						<div
							className={
								this.state.mobileNavOpen
									? 'navbar-menu is-active'
									: 'navbar-menu'
							}
						>
							<div className="navbar-end">
								<NavLink
									exact
									to="/"
									activeClassName="is-active"
									className="navbar-item"
									onClick={() => this.toggleNavMenu(false)}
								>
									Home
								</NavLink>
								<NavLink
									to="/performers"
									activeClassName="is-active"
									className="navbar-item"
									onClick={() => {
										this.toggleNavMenu(false);
										this.setPerformerFilter('');
									}}
								>
									Performers
								</NavLink>
								<NavLink
									to="/categories"
									activeClassName="is-active"
									className="navbar-item"
									onClick={() => this.toggleNavMenu(false)}
								>
									Entertainment Categories
								</NavLink>
							</div>
						</div>
					</div>
				</nav>
				<Switch>
					<Route
						exact
						path="/"
						render={(props) => (
							<Home
								{...props}
								featuredCategories={this.state.featuredCategories}
								featuredPerformers={this.state.featuredPerformers}
								categoriesLoading={this.state.categoriesLoading}
								performersLoading={this.state.performersLoading}
								setPerformerFilter={this.setPerformerFilter}
							/>
						)}
					/>
					<Route
						exact
						path="/performers"
						render={(props) => (
							<AllPerformers
								{...props}
								performersLoading={this.state.performersLoading}
								filteredPerformers={this.state.filteredPerformers}
								searchFilter={this.state.searchFilter}
								doPerformerFilter={this.doPerformerFilter}
							/>
						)}
					/>
					<Route
						path="/performers/:url"
						render={(props) => (
							<Performer
								{...props}
								performersLoading={this.state.performersLoading}
								allPerformers={this.state.allPerformers}
							/>
						)}
					/>
					<Route
						path="/categories"
						render={(props) => (
							<CategoryList
								{...props}
								setPerformerFilter={this.setPerformerFilter}
								allCategories={this.state.allCategories}
								categoriesLoading={this.state.categoriesLoading}
							/>
						)}
					/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
