<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Performers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('performer_model');
	}
	public function get_performers()
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->performer_model->get_performers()->result_array()));
	}
	public function get_performers_by_category($id)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->performer_model->get_performers_by_category($id)->result_array()));
	}
	public function get_performer($slug)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->performer_model->get_performer($slug)->result_array()));
	}
}
