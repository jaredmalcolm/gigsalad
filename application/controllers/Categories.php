<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
	}
	public function get_categories()
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->category_model->get_categories()->result_array()));
	}
	public function get_category($id)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->category_model->get_category($id)->result_array()));
	}
}
