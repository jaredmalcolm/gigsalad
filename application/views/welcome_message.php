<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>GigSalad Demo App</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.css">
	<style>
	dt {
		font-weight: bold;
	}
	</style>
</head>
<body>

<div id="app"></div>
<script src="/index.js"></script>
</body>
</html>